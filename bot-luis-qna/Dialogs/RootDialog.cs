﻿namespace bot_luis_qna.Dialogs
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;
    using System.Collections.Generic;
    using System.Threading;

    [Serializable]
    public class RootDialog : IDialog<object>
    {
        private static Random random = new Random();
        private const string recuperarSenha = "Recuperar Senha";
        private const string trocarSenha = "Trocar Senha";
        private const string outrosAtendimento = "Outros Atendimentos";

        private static string[] recepcaoAtendimento = {"Vou passar algumas opções para você.",
                                                       "Deixa eu te passar algumas opções de atendimento.",
                                                       "Olá, estou preparando algumas opções de atendimento."};

        private static string[] opcoesAtendimento = {"O que você quer fazer hoje?",
                                                     "Selecione uma das opções abaixo:",
                                                     "Que tal selecionar uma das opções abaixo:"};

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;

            var welcomeMessage = context.MakeMessage();
            welcomeMessage.Text = recepcaoAtendimento[random.Next(recepcaoAtendimento.Length)];
            await context.PostAsync(welcomeMessage.Text);
            Thread.Sleep(2000);
            await this.DisplayOptionsAsync(context);
        }

        public async Task DisplayOptionsAsync(IDialogContext context)
        {
            PromptDialog.Choice(
                context,
                this.AfterChoiceSelected,
                new[] { recuperarSenha, trocarSenha, outrosAtendimento },
                opcoesAtendimento[random.Next(opcoesAtendimento.Length)],
                "Desculpe, mas não entendi isso. Preciso que você selecione uma das opções abaixo",
                attempts: 2);
        }

        private async Task AfterChoiceSelected(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var selection = await result;

                switch (selection)
                {
                    case recuperarSenha:
                        await new PasswordDialog().StartAsync(context);
                        break;

                    case trocarSenha:
                        await context.PostAsync($"opção= {selection}");
                        break;

                    case outrosAtendimento:
                        await context.PostAsync($"opção= {selection}");
                        break;

                }
            }
            catch (TooManyAttemptsException)
            {
                await this.StartAsync(context);
            }
        }
    }
}